Flask Test
==========

Prerequisites
-------------

    sudo apt install python-setuptools
    pip install Flask
    sudo pip install flask-sqlalchemy

Maybe `sudo pip install SQLAlchemy` too. Not 100% sure.

Create database (use a proper one in production):

    sqlite3 feedback.db < feedback.sql
