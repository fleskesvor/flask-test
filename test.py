#!/usr/bin/python
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
# Use 4 slashes for absolute path
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///feedback.db'
db = SQLAlchemy(app)

class Feedback(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    helpful = db.Column(db.Integer)

    def __init__(self, helpful):
        self.helpful = helpful

@app.route("/feedback/<int:helpful>")
def feedback(helpful):
    fb = Feedback(helpful)
    db.session.add(fb)
    db.session.commit()
    return jsonify({"id": fb.id, "helpful": fb.helpful})
